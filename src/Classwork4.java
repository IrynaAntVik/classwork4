import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Classwork4 {

    public BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int n;
    int[] arr;
    int[] arrcopy;
    String code = null;

    public void dlinamassiva() {
        System.out.println("Введите длину массива");
        try {
            n = Integer.parseInt(reader.readLine());
            if (n <= 0) {
                System.out.println("Недопустимое значение. Длина массива должна быть целым положительным числом");
                dlinamassiva();
            }
        }
        catch (NumberFormatException n) {
            System.out.println("Недопустимое значение. Значение должно быть целым положительным числом");
            dlinamassiva();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void vvodmassiva() {
        try {
            arr = new int[n];
            for (int i = 0; i < n; i++) {
                System.out.println("Введите " + (i + 1) + "-й элемент");
                arr[i] = Integer.parseInt(reader.readLine());
            }
        }
        catch (NumberFormatException n) {
            System.out.println("Недопустимое значение. Значение должно быть целым числом");
            vvodmassiva();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void massivmin() {
        int y = arr [0];
        for (int i = 0; i < n - 1; i++) {
            if (y > arr [i+1]){
                y = arr [i+1];
            } else {
                continue;
            }
        }
        System.out.println(y + " - минимальный элемент массива");
    }

    public void massivmax() {
        int y = arr [0];
        for (int i = 0; i < n - 1; i++) {
            if (y > arr [i+1]){
                continue;
            } else {
                y = arr [i+1];
            }
        }
        System.out.println(y + " - максимальный элемент массива");
    }

    public void massivcopy() {
        for (int i = 0; i < n; i++) {
            arrcopy [i] = arr [i];
        }
        arrcopy = arr;
    }

    public void massivrevers() {
        for (int i = n -1; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public void sortPuz() {
        int x;
        boolean y = false;
        while (!y) {
            y = true;
            for (int i = 0; i < n - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    y = false;
                    x = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = x;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println("Сортировка пузырьком:");
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public void sortVstavka() {
        System.out.println("Сортировка вставкой:");
        int x;
        int j;
            for (int i = 0; i < n-1; i++) {
                if (arr[i] > arr[i + 1]) {
                    x = arr[i + 1];
                    arr[i + 1] = arr [i];
                    j = i;
                    while (j > 0 && x < arr [j-1]) {
                        arr[j] = arr[j-1];
                        j--;
                    }
                    arr[j] = x;
                }
            }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public void printMaster() {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public void printSecond() {
        for (int i = 0; i < n; i++) {
            System.out.print(arrcopy[i] + " ");
        }
        System.out.println("");
    }

    public void operacii() {
        try {
            do {
                System.out.println("Введите код операции");
                code = reader.readLine();
                switch (code) {
                    case "min":
                        massivmin();
                        break;
                    case "max":
                        massivmax();
                        break;
                    case "copy":
                        massivcopy();
                        break;
                    case "revers":
                        massivrevers();
                        break;
                    case "sortPuz":
                        sortPuz();
                        break;
                    case "sortVstavka":
                        sortVstavka();
                        break;
                    case "printMaster":
                        printMaster();
                        break;
                    case "printSecond":
                        if (arrcopy != arr) {
                        System.out.println("Невозможно вывести скопированный массив, т. к. команда copy еще не производилась");
                    } else {
                            printSecond();
                        }
                        break;
                    case "help":
                        System.out.println("Список доступных операций:");
                        System.out.println("min: определение минимального элемента массива");
                        System.out.println("max: определение максимального элемента массива");
                        System.out.println("copy: копирование массива");
                        System.out.println("revers: вывод элементов массива в обратном порядке");
                        System.out.println("sortPuz: сортировка элементов массива пузырьковым методом");
                        System.out.println("sortVstavka: сортировка элементов массива методом вставки");
                        System.out.println("printMaster: вывод элементов исходного массива");
                        System.out.println("printSecond: вывод элементов скопированного массива");
                        System.out.println("help: вывод списка доступных операций");
                        System.out.println("exit: выход из программы");
                        operacii();
                        break;
                    case "exit":
                        break;
                    default:
                        System.out.println("Неверный код операции. Введите help для вывода доступных операций");
                }
                reader.close();
            } while (!code.equals("exit"));
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
